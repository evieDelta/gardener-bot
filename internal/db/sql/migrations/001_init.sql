-- 2021-01-27
-- Initial migration for public schema
-- Holds information used by all modules

-- +migrate Up

create table public.guilds (
    id              bigint      primary key,
    admin_roles     bigint[]    not null   default array[]::bigint[],
    mod_roles       bigint[]    not null   default array[]::bigint[],
    helper_roles    bigint[]    not null   default array[]::bigint[]
);

create table public.scheduled_events (
    id          serial      primary key,
    event_type  text        not null,
    expires     timestamp   not null,
    data        jsonb       not null
);

create index public_scheduled_events_expires_idx on public.scheduled_events (expires);
create index public_scheduled_events_data_idx on public.scheduled_events using GIN (data);

create table public.config (
    id int primary key not null default 1, -- enforced only equal to 1

    status          text    not null    default 'online',
    activity_type   text    not null    default 'playing',
    activity        text    not null    default '',

    constraint singleton check (id = 1) -- enforce singleton table/row
);

insert into public.config (id) values (1);
