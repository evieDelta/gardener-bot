package db

import (
	"context"
	"embed"

	"codeberg.org/gardener/gardener-bot/internal/log"
	"emperror.dev/errors"
	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4/pgxpool"
	migrate "github.com/rubenv/sql-migrate"
)

type DB struct {
	*pgxpool.Pool

	migrator  *Migrator
	scheduler *Scheduler
}

//go:embed sql/migrations/*.sql
var migrations embed.FS

func init() {
	squirrel.StatementBuilder = squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)
}

func New(dsn string) (*DB, error) {
	pool, err := pgxpool.Connect(context.Background(), dsn)
	if err != nil {
		return nil, errors.Wrap(err, "creating pool")
	}

	db := &DB{
		Pool: pool,
		migrator: &Migrator{
			connString: dsn,
		},
		scheduler: NewScheduler(pool),
	}

	// can't ever return an error
	err = db.migrator.AddMigrations("public", migrate.EmbedFileSystemMigrationSource{
		FileSystem: migrations,
		Root:       "sql/migrations",
	})
	if err != nil {
		log.Fatalf("Error adding public migrations: %v", err)
	}

	return db, nil
}

func (db *DB) Migrator() MigratorInterface   { return db.migrator }
func (db *DB) Scheduler() SchedulerInterface { return db.scheduler }
