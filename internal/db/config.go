package db

import (
	"context"
	"strconv"

	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/jackc/pgx/v4"
)

type Config interface {
	GetGuildString(ctx context.Context, guildID discord.GuildID, key string) (val string, err error)
	GetGuildBool(ctx context.Context, guildID discord.GuildID, key string) (val bool, err error)
	GetGuildInt(ctx context.Context, guildID discord.GuildID, key string) (val int64, err error)

	SetGuildString(ctx context.Context, guildID discord.GuildID, key, val string) error
	SetGuildBool(ctx context.Context, guildID discord.GuildID, key string, val bool) error
	SetGuildInt(ctx context.Context, guildID discord.GuildID, key string, val int64) error
	DeleteGuildKey(ctx context.Context, guildID discord.GuildID, key string) error

	GetUserString(ctx context.Context, userID discord.UserID, key string) (val string, err error)
	GetUserBool(ctx context.Context, userID discord.UserID, key string) (val bool, err error)
	GetUserInt(ctx context.Context, userID discord.UserID, key string) (val int64, err error)

	SetUserString(ctx context.Context, userID discord.UserID, key, val string) error
	SetUserBool(ctx context.Context, userID discord.UserID, key string, val bool) error
	SetUserInt(ctx context.Context, userID discord.UserID, key string, val int64) error
	DeleteUserKey(ctx context.Context, userID discord.UserID, key string) error

	GetGuildUserString(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) (val string, err error)
	GetGuildUserBool(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) (val bool, err error)
	GetGuildUserInt(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) (val int64, err error)

	SetGuildUserString(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key, val string) error
	SetGuildUserBool(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string, val bool) error
	SetGuildUserInt(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string, val int64) error
	DeleteGuildUserKey(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) error
}

func (db *DB) GetGuildString(ctx context.Context, guildID discord.GuildID, key string) (val string, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, '') from guild_config where guild_id = $2", key, guildID).Scan(&val)
	if err == pgx.ErrNoRows {
		return "", nil
	}
	return val, err
}

func (db *DB) GetGuildBool(ctx context.Context, guildID discord.GuildID, key string) (val bool, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, 'false')::boolean from guild_config where guild_id = $2", key, guildID).Scan(&val)
	if err == pgx.ErrNoRows {
		return false, nil
	}
	return val, err
}

func (db *DB) GetGuildInt(ctx context.Context, guildID discord.GuildID, key string) (val int64, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, '0')::bigint from guild_config where guild_id = $2", key, guildID).Scan(&val)
	if err == pgx.ErrNoRows {
		return 0, nil
	}
	return val, err
}

func (db *DB) SetGuildString(ctx context.Context, guildID discord.GuildID, key, val string) error {
	sql := `insert into guild_config (guild_id, config)
	values ($1, hstore($2, $3))
	on conflict (guild_id) do update
	set config = guild_config.config || hstore($2, $3)`

	_, err := db.Pool.Exec(ctx, sql, guildID, key, val)
	return err
}

func (db *DB) SetGuildBool(ctx context.Context, guildID discord.GuildID, key string, val bool) error {
	sql := `insert into guild_config (guild_id, config)
	values ($1, hstore($2, $3))
	on conflict (guild_id) do update
	set config = guild_config.config || hstore($2, $3)`

	_, err := db.Pool.Exec(ctx, sql, guildID, key, strconv.FormatBool(val))
	return err
}

func (db *DB) SetGuildInt(ctx context.Context, guildID discord.GuildID, key string, val int64) error {
	sql := `insert into guild_config (guild_id, config)
	values ($1, hstore($2, $3))
	on conflict (guild_id) do update
	set config = guild_config.config || hstore($2, $3)`

	_, err := db.Pool.Exec(ctx, sql, guildID, key, strconv.FormatInt(val, 10))
	return err
}

func (db *DB) DeleteGuildKey(ctx context.Context, guildID discord.GuildID, key string) error {
	sql := `insert into guild_config (guild_id, config)
	values ($1, ''::hstore)
	on conflict (guild_id) do update
	set config = delete(guild_config.config, $2)`

	_, err := db.Pool.Exec(ctx, sql, guildID, key)
	return err
}

func (db *DB) GetUserString(ctx context.Context, userID discord.UserID, key string) (val string, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, '') from user_config where user_id = $2", key, userID).Scan(&val)
	if err == pgx.ErrNoRows {
		return "", nil
	}
	return val, err
}

func (db *DB) GetUserBool(ctx context.Context, userID discord.UserID, key string) (val bool, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, 'false')::boolean from user_config where user_id = $2", key, userID).Scan(&val)
	if err == pgx.ErrNoRows {
		return false, nil
	}
	return val, err
}

func (db *DB) GetUserInt(ctx context.Context, userID discord.UserID, key string) (val int64, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, '0')::bigint from user_config where user_id = $2", key, userID).Scan(&val)
	if err == pgx.ErrNoRows {
		return 0, nil
	}
	return val, err
}

func (db *DB) SetUserString(ctx context.Context, userID discord.UserID, key, val string) error {
	sql := `insert into user_config (user_id, config)
	values ($1, hstore($2, $3))
	on conflict (user_id) do update
	set config = user_config.config || hstore($2, $3)`

	_, err := db.Pool.Exec(ctx, sql, userID, key, val)
	return err
}

func (db *DB) SetUserBool(ctx context.Context, userID discord.UserID, key string, val bool) error {
	sql := `insert into user_config (user_id, config)
	values ($1, hstore($2, $3))
	on conflict (user_id) do update
	set config = user_config.config || hstore($2, $3)`

	_, err := db.Pool.Exec(ctx, sql, userID, key, strconv.FormatBool(val))
	return err
}

func (db *DB) SetUserInt(ctx context.Context, userID discord.UserID, key string, val int64) error {
	sql := `insert into user_config (user_id, config)
	values ($1, hstore($2, $3))
	on conflict (user_id) do update
	set config = user_config.config || hstore($2, $3)`

	_, err := db.Pool.Exec(ctx, sql, userID, key, strconv.FormatInt(val, 10))
	return err
}

func (db *DB) DeleteUserKey(ctx context.Context, userID discord.UserID, key string) error {
	sql := `insert into user_config (user_id, config)
	values ($1, ''::hstore)
	on conflict (user_id) do update
	set config = delete(user_config.config, $2)`

	_, err := db.Pool.Exec(ctx, sql, userID, key)
	return err
}

func (db *DB) GetGuildUserString(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) (val string, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, '') from user_guild_config where user_id = $2 and guild_id = $3", key, userID, guildID).Scan(&val)
	if err == pgx.ErrNoRows {
		return "", nil
	}
	return val, err
}

func (db *DB) GetGuildUserBool(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) (val bool, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, 'false')::boolean from user_guild_config where user_id = $2 and guild_id = $3", key, userID, guildID).Scan(&val)
	if err == pgx.ErrNoRows {
		return false, nil
	}
	return val, err
}

func (db *DB) GetGuildUserInt(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) (val int64, err error) {
	err = db.Pool.QueryRow(ctx, "select coalesce(config->$1, '0')::bigint from user_guild_config where user_id = $2 and guild_id = $3", key, userID, guildID).Scan(&val)
	if err == pgx.ErrNoRows {
		return 0, nil
	}
	return val, err
}

func (db *DB) SetGuildUserString(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key, val string) error {
	sql := `insert into user_guild_config (user_id, guild_id, config)
	values ($1, $2, hstore($3, $4))
	on conflict (user_id, guild_id) do update
	set config = user_guild_config.config || hstore($3, $4)`

	_, err := db.Pool.Exec(ctx, sql, userID, guildID, key, val)
	return err
}

func (db *DB) SetGuildUserBool(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string, val bool) error {
	sql := `insert into user_guild_config (user_id, guild_id, config)
	values ($1, $2, hstore($3, $4))
	on conflict (user_id, guild_id) do update
	set config = user_guild_config.config || hstore($3, $4)`

	_, err := db.Pool.Exec(ctx, sql, userID, guildID, key, strconv.FormatBool(val))
	return err
}

func (db *DB) SetGuildUserInt(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string, val int64) error {
	sql := `insert into user_guild_config (user_id, guild_id, config)
	values ($1, $2, hstore($3, $4))
	on conflict (user_id, guild_id) do update
	set config = user_guild_config.config || hstore($3, $4)`

	_, err := db.Pool.Exec(ctx, sql, userID, guildID, key, strconv.FormatInt(val, 10))
	return err
}

func (db *DB) DeleteGuildUserKey(ctx context.Context, guildID discord.GuildID, userID discord.UserID, key string) error {
	sql := `insert into user_guild_config (user_id, guild_id, config)
	values ($1, $2, ''::hstore)
	on conflict (user_id, guild_id) do update
	set config = delete(user_guild_config.config, $3)`

	_, err := db.Pool.Exec(ctx, sql, userID, guildID, key)
	return err
}
