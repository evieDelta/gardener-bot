package bot

import (
	"codeberg.org/gardener/gardener-bot/internal/config"
	"codeberg.org/gardener/gardener-bot/internal/log"
)

type HasConfig interface {
	// SetupConfig should return a pointer to the configuration struct,
	// and a bool indicating whether the configuration file is required.
	//
	// The file name will be the module's Name() + the .yaml file extension.
	SetupConfig() (v any, required bool)
}

func readModuleConfig(m Module) error {
	cfg, ok := m.(HasConfig)
	if !ok {
		return nil
	}

	filename := m.Name() + ".yaml"
	v, required := cfg.SetupConfig()

	log.Debugf("Reading configuration file for module %q (filename: %q, required: %v)", m.Name(), filename, required)

	err := config.Unmarshal(filename, v)
	if err != nil && required {
		return err
	}
	return nil
}
