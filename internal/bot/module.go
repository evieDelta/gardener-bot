package bot

import (
	"codeberg.org/gardener/gardener-bot/internal/db"
	"github.com/diamondburned/arikawa/v3/api"
	migrate "github.com/rubenv/sql-migrate"
)

// Module is a module.
// It must embed ModuleBase in this package.
//
// Modules may also implement the following other interfaces:
// - HasConfig
type Module interface {
	init(*Bot) // implemented by ModuleBase

	// Name is used for the configuration file (suffixed with .yaml), database schema, and help command.
	// It should be lowercase.
	Name() string
	// Migrations are added to the database's Migrator.
	// nil if the module has no migrations.
	Migrations() migrate.MigrationSource

	// Setup is used to register aliases, handlers etc.
	// Most modules can just use Commands, AppCommands, Handlers, and EventTypes instead.
	Setup() error

	// Ready is called *in a separate goroutine* when the bot receives its first ready event
	Ready()

	// TODO: replace with slice of command structs
	Commands() []any

	// List of application commands to register with Discord
	AppCommands() []api.CreateCommandData

	// List of handlers to add to the bot
	Handlers() []any

	// EventTypes are registered to the database's Scheduler
	EventTypes() []db.Event
}

// ModuleBase is the base for a module.
// It must be embedded into bot module types (usually called "Module" as well)
type ModuleBase struct {
	*Bot
}

func (m *ModuleBase) init(bot *Bot) {
	m.Bot = bot
}

// Methods that modules can override, defined here so that unused methods don't have to be added to modules.
func (*ModuleBase) Migrations() migrate.MigrationSource  { return nil }
func (*ModuleBase) Setup() error                         { return nil }
func (*ModuleBase) Ready()                               {}
func (*ModuleBase) Commands() []any                      { return nil }
func (*ModuleBase) AppCommands() []api.CreateCommandData { return nil }
func (*ModuleBase) Handlers() []any                      { return nil }
func (*ModuleBase) EventTypes() []db.Event               { return nil }
