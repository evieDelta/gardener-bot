package config

import (
	"os"
	"path/filepath"

	"codeberg.org/gardener/gardener-bot/internal/log"
	"emperror.dev/errors"
	"gopkg.in/yaml.v3"
)

var rootPath = "./config"

func SetRoot(s string) { rootPath = s }

// Unmarshal unmarshals the given file into v.
// It accepts yaml files.
func Unmarshal(filename string, v any) error {
	path := filepath.Join(rootPath, filename)
	log.Infof("Loading config file at %q", path)

	b, err := os.ReadFile(path)
	if err != nil {
		return errors.Wrap(err, "config.Unmarshal: reading file")
	}

	err = yaml.Unmarshal(b, v)
	if err != nil {
		return errors.Wrap(err, "config.Unmarshal: unmarshal yaml")
	}

	return nil
}
