package config

import "github.com/diamondburned/arikawa/v3/discord"

type Config struct {
	Auth    AuthConfig    `yaml:"auth"`
	Discord DiscordConfig `yaml:"discord"`
}

type AuthConfig struct {
	Token    string `yaml:"token"`
	Database string `yaml:"database"`
}

type DiscordConfig struct {
	OverwriteCommands bool            `yaml:"overwrite_commands"`
	CommandsGuild     discord.GuildID `yaml:"commands_guild"`
}
