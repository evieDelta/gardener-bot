#!/bin/sh
CGO_ENABLED=0 go build -o gardener -v -ldflags="-buildid= -X codeberg.org/gardener/gardener-bot/internal.Version=`git rev-parse --short HEAD`" ./cmd/gardener
