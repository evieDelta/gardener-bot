package report

import (
	"sync"
	"time"

	"codeberg.org/gardener/gardener-bot/bot"
	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
)

// The timeout before a message can be reported again
const timeout = 5 * time.Minute

const defaultReportError = "There was an error sending your report! Please contact the moderators."
const defaultReportOK = "I've forwarded your report!"

// Module is the reports module
type Module struct {
	bot.ModuleBase

	msgs   map[discord.MessageID]time.Time
	msgsMu sync.Mutex
}

func (*Module) Name() string { return "reports" }

func (*Module) AppCommands() []api.CreateCommandData {
	return []api.CreateCommandData{
		{
			Type:        discord.ChatInputCommand,
			Name:        "report",
			Description: "Report something in chat",
			Options: discord.CommandOptions{
				&discord.StringOption{
					OptionName:  "reason",
					Description: "The reason you're reporting the current chat",
					Required:    true,
				},
			},
		},
		{
			Type:                     discord.ChatInputCommand,
			Name:                     "reportconfig",
			Description:              "Configure chat reports",
			DefaultMemberPermissions: discord.NewPermissions(discord.PermissionManageGuild),
			Options: discord.CommandOptions{
				&discord.SubcommandOption{
					OptionName:  "channel",
					Description: "Set the channel reports are sent to",
					Options: []discord.CommandOptionValue{
						&discord.ChannelOption{
							OptionName:   "channel",
							Description:  "The channel reports are sent to",
							Required:     true,
							ChannelTypes: []discord.ChannelType{discord.GuildText},
						},
					},
				},
				&discord.SubcommandGroupOption{
					OptionName:  "message",
					Description: "Set the message sent when reports are sent",
					Subcommands: []*discord.SubcommandOption{
						{
							OptionName:  "success",
							Description: "Set the message sent when a report is successful",
							Options: []discord.CommandOptionValue{&discord.StringOption{
								OptionName:  "message",
								Description: "The message sent when a report is successful",
								Required:    true,
							}},
						},
						{
							OptionName:  "error",
							Description: "Set the message sent when a report is unsuccessful",
							Options: []discord.CommandOptionValue{&discord.StringOption{
								OptionName:  "message",
								Description: "The message sent when a report is unsuccessful",
								Required:    true,
							}},
						},
					},
				},
			},
		},
		{
			Type: discord.MessageCommand,
			Name: "Report message",
		},
	}
}

func (m *Module) Setup() error {
	m.msgs = make(map[discord.MessageID]time.Time)

	m.Bot.Interactions.Command("report").Exec(m.report)
	m.Bot.Interactions.Command("Report message").Exec(m.reportMsg)

	m.Bot.Interactions.Command("reportconfig/channel").Exec(m.configChannel)
	m.Bot.Interactions.Command("reportconfig/message/success").Exec(m.configSuccessMessage)
	m.Bot.Interactions.Command("reportconfig/message/error").Exec(m.configErrorMessage)
	return nil
}
