-- 2023-01-21
-- Initial levels migration

-- +migrate Up

create table if not exists levels.guilds (
    id  bigint  primary key,

    blocked_channels    bigint[]    not null    default array[]::bigint[],
    blocked_roles       bigint[]    not null    default array[]::bigint[],
    blocked_categories  bigint[]    not null    default array[]::bigint[],

    between_xp  interval    not null    default '1 minute',
    reward_text text        not null    default ''
);

create table if not exists levels.levels (
    guild_id    bigint  not null,
    user_id     bigint  not null,

    xp          bigint  not null    default 0,
    colour      bigint  not null    default 0,
    background  text    not null    default '',

    last_xp     timestamp   not null    default (current_timestamp at time zone 'utc'),

    primary key (guild_id, user_id)
);

create table if not exists levels.rewards (
    guild_id    bigint  not null,
    lvl         bigint  not null,
    role_reward bigint  not null,

    primary key (guild_id, lvl)
);

create table if not exists levels.blocks (
    guild_id    bigint      not null,
    user_id     bigint      not null,
    expiry      timestamp   default (current_timestamp at time zone 'utc'),

    primary key (guild_id, user_id)
);
