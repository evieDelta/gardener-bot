// Package levels implements a sample module for the Gardener.
package levels

import (
	"embed"

	"codeberg.org/gardener/gardener-bot/internal/bot"
	"codeberg.org/gardener/gardener-bot/internal/db"
	migrate "github.com/rubenv/sql-migrate"
)

const (
	keyEnabled                = "levels:enabled"
	keyLeaderboardRestricted  = "levels:leaderboard_mod_only"
	keyLevelCommandRestricted = "levels:level_cmd_mod_only"

	keyRewardLog   = "levels:reward_log"
	keyNolevelsLog = "levels:nolevels_log"

	keyBackground = "levels:background"
)

//go:embed migrations/*.sql
var fs embed.FS

var _ bot.Module = (*Module)(nil)

// Module is the levels module.
type Module struct {
	bot.ModuleBase
}

func (*Module) Name() string { return "levels" }

func (*Module) Migrations() migrate.MigrationSource {
	return &migrate.EmbedFileSystemMigrationSource{
		FileSystem: fs,
		Root:       "migrations",
	}
}

func (*Module) EventTypes() []db.Event {
	return []db.Event{
		&levelBlock{},
	}
}
