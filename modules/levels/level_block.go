package levels

import (
	"context"
	"fmt"
	"time"

	"codeberg.org/gardener/gardener-bot/internal"
	"codeberg.org/gardener/gardener-bot/internal/db"
	"codeberg.org/gardener/gardener-bot/internal/log"
	"github.com/diamondburned/arikawa/v3/discord"
)

type levelBlock struct {
	GuildID     discord.GuildID `json:"guild_id"`
	UserID      discord.UserID  `json:"user_id"`
	ModeratorID discord.UserID  `json:"moderator"`
	Set         time.Time       `json:"set"`
}

func (dat *levelBlock) Execute(ctx context.Context, id int64, bot *db.Bot) error {
	log.Debugf("Removing level block for %d (event id: %d)", dat.UserID, id)

	_, err := bot.DB.Exec(ctx, "delete from levels.blocks where guild_id = $1 and user_id = $2", dat.GuildID, dat.UserID)
	if err != nil {
		log.Errorf("Error deleting level block: %v", err)
		return db.Reschedule
	}

	var logChannel discord.ChannelID
	err = bot.DB.QueryRow(ctx, "select nolevels_log from levels.guilds where id = $1", dat.GuildID).Scan(&logChannel)
	if err != nil {
		log.Errorf("Error getting level block log for guild %v: %v", dat.GuildID, err)
		return err
	}

	if !logChannel.IsValid() {
		return nil
	}

	var user, moderator string
	u, err := bot.State.User(dat.UserID)
	if err == nil {
		user = fmt.Sprintf("%v (%v %v)", u.Tag(), u.Mention(), u.ID)
	} else {
		user = fmt.Sprintf("%v (%v)", dat.UserID.Mention(), dat.UserID)
	}

	u, err = bot.State.User(dat.ModeratorID)
	if err == nil {
		moderator = fmt.Sprintf("%v (%v %v)", u.Tag(), u.Mention(), u.ID)
	} else {
		moderator = fmt.Sprintf("%v (%v)", dat.ModeratorID.Mention(), dat.ModeratorID)
	}

	e := discord.Embed{
		Title:       "Level block expired",
		Description: fmt.Sprintf("**User:** %v\n**Moderator:** %v\n**Blocked at:** <t:%v>", user, moderator, dat.Set.Unix()),
		Timestamp:   discord.Timestamp(dat.Set),
		Color:       internal.ColourBlurple,
	}

	_, err = bot.State.SendEmbeds(logChannel, e)
	if err != nil {
		log.Errorf("Error sending level block log: %v", err)
	}
	return err
}

func (*levelBlock) Offset() time.Duration { return time.Minute }
