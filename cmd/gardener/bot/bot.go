package bot

import (
	"codeberg.org/gardener/gardener-bot/bot"
	"codeberg.org/gardener/gardener-bot/modules/levels"
	"codeberg.org/gardener/gardener-bot/modules/report"
	"github.com/urfave/cli/v2"
)

var Command = &cli.Command{
	Name:   "bot",
	Usage:  "Run the bot",
	Action: run,
}

func run(c *cli.Context) (err error) {
	bot.Run(c.Context,
		&levels.Module{},
		&report.Module{},
	)
	return nil
}
