FROM golang:latest AS builder

WORKDIR /build
COPY . ./
RUN go mod download
ENV CGO_ENABLED 0
RUN go build -o gardener -v -ldflags="-buildid= -X codeberg.org/gardener/gardener-bot/internal.Version=`git rev-parse --short HEAD`" ./cmd/gardener

FROM alpine:latest

RUN apk --no-cache add ca-certificates tzdata

WORKDIR /app
COPY --from=builder /build/gardener gardener

CMD ["/app/gardener", "bot"]
