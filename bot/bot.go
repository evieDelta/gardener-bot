package bot

import (
	"context"
	"os"
	"os/signal"

	"codeberg.org/gardener/gardener-bot/internal/bot"
	"codeberg.org/gardener/gardener-bot/internal/config"
	"codeberg.org/gardener/gardener-bot/internal/log"
)

type (
	Bot        = bot.Bot
	Module     = bot.Module
	Database   = bot.Database
	Migrator   = bot.Migrator
	Scheduler  = bot.Scheduler
	ModuleBase = bot.ModuleBase
	HasConfig  = bot.HasConfig
)

func Run(ctx context.Context, m ...Module) {
	var conf config.Config

	err := config.Unmarshal("gardener.yaml", &conf)
	if err != nil {
		log.Fatal(err)
	}

	// override database URL, if the environment variable is set
	// (for Docker)
	if s := os.Getenv("GARDENER_DATABASE"); s != "" {
		conf.Auth.Database = s
	}

	b, err := bot.New(conf)
	if err != nil {
		log.Fatalf("creating bot: %v", err)
	}

	b.Modules = append(b.Modules, m...)

	cctx, cancel := signal.NotifyContext(ctx, os.Interrupt)
	defer cancel()

	err = b.Start(cctx)
	if err != nil {
		log.Fatalf("opening connection to Discord: %v", err)
	}

	log.Info("Bot is now running!")

	<-cctx.Done()
	log.Info("Interrupt signal received. Shutting down...")
}
